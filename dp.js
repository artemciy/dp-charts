function y20(month, day) { return new Date(2020, month - 1, day) }

// The first chart is a mix between trying and achieving
// Like Dweck [mentioned](https://youtu.be/J-swZaKN2Ic), every time we try something,
// we're a step closer to getting there, so trying also counts
// 0 .. no count (didn't try or forgot to mention on the chart)
// 1 .. definitely tried something
// 2 .. made some progress
// 3 .. definite improvement was achieved through practice

// The scales represent something that likely needs a recurring effort
// (Take https://en.wikipedia.org/wiki/Attachment_in_adults#Affect_regulation for example,
// where the “Partner Responds Positively” needs to be recurring in order for affect regulation to work)
// It's like driving a car: every day to get around we need to invest time and effort into
// orienting on the road and stirring the wheel, etc

// The scales are generic, there's a ton of specific techniques and activities for each of them,
// most of which are tracked elsewhere

// TODO: Switch to generating the DP graph(s) in a PWA and from the replicated `agenda` stats

streamgraph('#a-proc', window.innerWidth - 10, 200, d3.schemeAccent, [
  //{ day: y20(0, 0), entropy: 0, orientation: 0, wellness: 0, fireworks: 0 },
  { day: y20(09, 16), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(09, 15), entropy: 2, orientation: 2, wellness: 1, fireworks: 2 },  // HearMe
  { day: y20(09, 14), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(09, 13), entropy: 2, orientation: 3, wellness: 1, fireworks: 3 },  // fire
  { day: y20(09, 12), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(09, 11), entropy: 1, orientation: 3, wellness: 1, fireworks: 2 },
  { day: y20(09, 10), entropy: 1, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(09, 09), entropy: 2, orientation: 3, wellness: 2, fireworks: 2 },
  { day: y20(09, 08), entropy: 1, orientation: 3, wellness: 2, fireworks: 1 },  // agenda.rs
  { day: y20(09, 07), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(09, 06), entropy: 2, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(09, 05), entropy: 2, orientation: 3, wellness: 1, fireworks: 2 },  // up-chem
  { day: y20(09, 04), entropy: 2, orientation: 2, wellness: 2, fireworks: 1 },
  { day: y20(09, 03), entropy: 1, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(09, 02), entropy: 1, orientation: 1, wellness: 1, fireworks: 3 },  // touch
  { day: y20(09, 01), entropy: 1, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(08, 31), entropy: 3, orientation: 3, wellness: 1, fireworks: 1 },
  { day: y20(08, 30), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(08, 29), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(08, 28), entropy: 2, orientation: 2, wellness: 2, fireworks: 3 },  // rainbow
  { day: y20(08, 27), entropy: 1, orientation: 2, wellness: 1, fireworks: 3 },  // love
  { day: y20(08, 26), entropy: 1, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(08, 25), entropy: 2, orientation: 2, wellness: 1, fireworks: 3 },
  { day: y20(08, 24), entropy: 2, orientation: 3, wellness: 1, fireworks: 2 },
  { day: y20(08, 23), entropy: 1, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(08, 22), entropy: 1, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(08, 21), entropy: 1, orientation: 1, wellness: 0, fireworks: 1 },
  { day: y20(08, 20), entropy: 2, orientation: 1, wellness: 1, fireworks: 3 },
  { day: y20(08, 19), entropy: 1, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(08, 18), entropy: 1, orientation: 1, wellness: 2, fireworks: 2 },
  { day: y20(08, 17), entropy: 2, orientation: 2, wellness: 1, fireworks: 1 },
  { day: y20(08, 16), entropy: 2, orientation: 2, wellness: 1, fireworks: 3 },  // dream sex
  { day: y20(08, 15), entropy: 2, orientation: 2, wellness: 0, fireworks: 1 },
  { day: y20(08, 14), entropy: 2, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(08, 13), entropy: 2, orientation: 2, wellness: 1, fireworks: 1 },
  { day: y20(08, 12), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(08, 11), entropy: 2, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(08, 10), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(08, 09), entropy: 2, orientation: 1, wellness: 0, fireworks: 2 },
  { day: y20(08, 08), entropy: 2, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(08, 07), entropy: 1, orientation: 1, wellness: 2, fireworks: 1 },
  { day: y20(08, 06), entropy: 2, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(08, 05), entropy: 3, orientation: 1, wellness: 1, fireworks: 3 },
  { day: y20(08, 04), entropy: 3, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(08, 03), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(08, 02), entropy: 3, orientation: 2, wellness: 2, fireworks: 3 },
  { day: y20(08, 01), entropy: 3, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(07, 31), entropy: 3, orientation: 3, wellness: 2, fireworks: 3 },
  { day: y20(07, 30), entropy: 2, orientation: 2, wellness: 1, fireworks: 1 },
  { day: y20(07, 29), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(07, 28), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(07, 27), entropy: 1, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(07, 26), entropy: 1, orientation: 1, wellness: 2, fireworks: 1 },
  { day: y20(07, 25), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 24), entropy: 1, orientation: 1, wellness: 2, fireworks: 2 },
  { day: y20(07, 23), entropy: 1, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(07, 22), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 21), entropy: 2, orientation: 2, wellness: 1, fireworks: 3 },  // instant dream-sight
  { day: y20(07, 20), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 19), entropy: 1, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(07, 18), entropy: 2, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(07, 17), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 16), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 15), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 14), entropy: 2, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(07, 13), entropy: 3, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(07, 12), entropy: 1, orientation: 1, wellness: 1, fireworks: 3 },
  { day: y20(07, 11), entropy: 2, orientation: 1, wellness: 2, fireworks: 3 },
  { day: y20(07, 10), entropy: 2, orientation: 3, wellness: 1, fireworks: 2 },
  { day: y20(07, 09), entropy: 1, orientation: 1, wellness: 2, fireworks: 2 },
  { day: y20(07, 08), entropy: 3, orientation: 3, wellness: 1, fireworks: 2 },
  { day: y20(07, 07), entropy: 2, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(07, 06), entropy: 2, orientation: 2, wellness: 1, fireworks: 1 },
  { day: y20(07, 05), entropy: 3, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(07, 04), entropy: 2, orientation: 1, wellness: 1, fireworks: 3 },
  { day: y20(07, 03), entropy: 2, orientation: 2, wellness: 1, fireworks: 3 },
  { day: y20(07, 02), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(07, 01), entropy: 2, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(06, 30), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 29), entropy: 3, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(06, 28), entropy: 2, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(06, 27), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 26), entropy: 2, orientation: 1, wellness: 1, fireworks: 3 },
  { day: y20(06, 25), entropy: 3, orientation: 2, wellness: 1, fireworks: 1 },
  { day: y20(06, 24), entropy: 2, orientation: 2, wellness: 1, fireworks: 1 },
  { day: y20(06, 23), entropy: 3, orientation: 2, wellness: 1, fireworks: 2 },
  { day: y20(06, 22), entropy: 2, orientation: 2, wellness: 2, fireworks: 1 },
  { day: y20(06, 21), entropy: 2, orientation: 1, wellness: 2, fireworks: 2 },
  { day: y20(06, 20), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 19), entropy: 1, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 18), entropy: 3, orientation: 1, wellness: 1, fireworks: 2 },
  { day: y20(06, 17), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 16), entropy: 3, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 15), entropy: 2, orientation: 3, wellness: 2, fireworks: 3 },
  { day: y20(06, 14), entropy: 2, orientation: 2, wellness: 1, fireworks: 3 },
  { day: y20(06, 13), entropy: 2, orientation: 2, wellness: 2, fireworks: 1 },
  { day: y20(06, 12), entropy: 1, orientation: 2, wellness: 0, fireworks: 3 },
  { day: y20(06, 11), entropy: 3, orientation: 2, wellness: 3, fireworks: 2 },
  { day: y20(06, 10), entropy: 2, orientation: 1, wellness: 1, fireworks: 1 },
  { day: y20(06, 09), entropy: 3, orientation: 3, wellness: 2, fireworks: 1 },
  { day: y20(06, 08), entropy: 2, orientation: 1, wellness: 1, fireworks: 3 },
  { day: y20(06, 07), entropy: 2, orientation: 2, wellness: 1, fireworks: 3 },
  { day: y20(06, 06), entropy: 2, orientation: 2, wellness: 2, fireworks: 3 },
  { day: y20(06, 05), entropy: 3, orientation: 2, wellness: 2, fireworks: 2 },
  { day: y20(06, 04), entropy: 3, orientation: 1, wellness: 2, fireworks: 3 },
  { day: y20(06, 03), entropy: 1, orientation: 0, wellness: 1, fireworks: 1 }
]);

function streamgraph(select, aw, ah, scheme, data) {
  // Collect the keys used in `data`
  var keys = [];
  for (let ix = 0; ix < data.length; ++ix) {
    const el = data[ix];
    for (var key in el) {
      if (!el.hasOwnProperty(key)) continue;
      if (key == 'day') continue;
      if (keys.includes(key)) continue;
      keys.push(key);
    }
  }

  // Add missing `data` keys
  for (let ix = 0; ix < data.length; ++ix) {
    const el = data[ix];
    for (const key of keys) {
      if (el.hasOwnProperty(key)) continue;
      el[key] = 0
    }
  }

  // https://www.d3-graph-gallery.com/graph/streamgraph_template.html
  // https://github.com/d3/d3/blob/master/API.md#stacks

  const margin = { top: 10, right: 10, bottom: 10, left: 40 },
    width = aw - margin.left - margin.right,
    height = ah - margin.top - margin.bottom;

  const svg = d3.select(select)
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  const stack = d3.stack()
    .keys(keys)
    .offset(d3.stackOffsetSilhouette);

  const series = stack(data);

  const x = d3.scaleTime()
    .domain(d3.extent(data, d => d.day))
    .range([5, width]);

  svg.append("g")
    .attr("transform", "translate(0," + height * 0.97 + ")")
    .call(d3.axisBottom(x).ticks(3));

  const y = d3.scaleLinear()
    .domain([-7, 7])
    .range([height, 0]);

  svg.append("g")
    .call(d3.axisLeft(y));

  const Tooltip = svg
    .append("text")
    .attr("x", 5)
    .attr("y", 3)
    .style("opacity", 0)
    .style("font-size", 17)

  const mouseover = function (d) {
    Tooltip.style("opacity", 1)
    d3.selectAll(".myArea").style("opacity", .2)
    d3.select(this)
      .style("stroke", "black")
      .style("opacity", 1)
  }
  const mousemove = function (d, i) {
    grp = keys[i]
    Tooltip.text(grp)
  }
  const mouseleave = function (d) {
    Tooltip.style("opacity", 0)
    d3.selectAll(".myArea").style("opacity", 1).style("stroke", "none")
  }

  const area = d3.area()
    .x(d => x(d.data.day))
    .y0(d => y(d[0]))
    .y1(d => y(d[1]));

  const color = d3.scaleOrdinal()
    .domain(keys)
    .range(scheme);

  svg
    .selectAll("mylayers")
    .data(series)
    .enter()
    .append("path")
    .attr("class", "myArea")
    .style("fill", function (d) { return color(d.key); })
    .attr("d", area)
    .on("mouseover", mouseover)
    .on("mousemove", mousemove)
    .on("mouseleave", mouseleave);
}